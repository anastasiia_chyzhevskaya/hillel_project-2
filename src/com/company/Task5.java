package com.company;

public class Task5 {

    public static void main(String[] args) {
        String arr[] = {"apple", "banana", "blackberry", "blueberry"};

        //for each loop
        for (String x : arr) {
            System.out.print(x);
            System.out.print(",");
        }

        System.out.println(arr.length + " first length - количество строк в массиве");

        System.out.println();

        System.out.println(arr[0].length() + " array length");
        System.out.println(arr[1].length() + " array length");
        //do_while loop
        int i = 0;
        do {
            System.out.println("Значение х: " + arr[i]);
            i++;
            System.out.println("\n");

        } while (i < arr.length);
    }
}

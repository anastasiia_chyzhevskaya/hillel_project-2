package com.company;

import java.util.Scanner;

public class Task4 {

    public static void main(String[] args) {
        Scanner scr = new Scanner(System.in);
        System.out.println("Введите Ваш возраст:");
        if (scr.hasNextInt()) {
            System.out.println( scr.nextInt() >= 18 ? "Вы можете голосовать":"Вы не можете голосовать");

        } else {
            System.out.println("Извините, но это явно не число. Перезапустите программу и попробуйте снова!");
        }
    }
}
package com.company;

public class Task2 {

    public static void task2 (){
        //TASK 2
        int arr[] = {4, 9, 15, 7};
        //do_while loop
        int i = 0;
        do {
            System.out.println("Значение х: " + arr[i]);
            i++;
            System.out.println("\n");

        } while (i < arr.length);

        //for loop
        int j;
        for (j = 0; j < arr.length; j++) {
            System.out.print("Значение х: " + arr[j]);
            System.out.println("\n");
        }

        //for_each loop
        for (int x : arr) {
            System.out.print(x);
            System.out.print(",");
        }
    }

}

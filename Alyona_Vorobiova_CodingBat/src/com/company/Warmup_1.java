package com.company;

public class Warmup_1 {


    public boolean sleepIn(boolean weekday, boolean vacation) {
        if (weekday == false || vacation == true) {
            return true;
        } else return false;
    }

    public boolean monkeyTrouble(boolean aSmile, boolean bSmile) {
        if (aSmile ==false && bSmile == false || aSmile ==true && bSmile ==true) {
            return true;
        }
        else return false;
    }

    public int sumDouble(int a, int b) {
        return a == b ? (a + b) * 2 : a + b;
    }

    public int diff21(int n) {
        return n<= 21 ? (21 -n) : 2*(n -21);
    }
    public boolean parrotTrouble(boolean talking, int hour) {
        if ( talking == true && (hour < 7 || hour > 20)) {
            return true;
        }
        else return false;
    }

    public boolean makes10(int a, int b) {
        return a + b ==10 || a ==10 || b==10;
    }

    public boolean nearHundred(int n) {
        return ((Math.abs(100 - n) <= 10) ||
                (Math.abs(200 - n) <= 10));
    }

    public boolean posNeg(int a, int b, boolean negative) {
        return ((a > 0 && b < 0 || a < 0 && b>0)&& negative ==false)||(negative ==true && a <0 &&b<0);
    }

    public String notString(String str) {
        if (str.length() >=3 && str.substring(0,3).equals("not"))  {
            return str;
        }
        else return ("not " + str);
    }


    public String missingChar(String str, int n) {
        return ( str.substring(0,n) +str.substring(n+1,str.length()));
    }

    public String frontBack(String str) {
        if (str.length() < 2)
        {
            return str;
        }
        else return (str.charAt(str.length()-1)+str.substring(1,str.length() -1) +str.charAt(0));
    }



    public String front3(String str) {
        if (str.length() <=3) return str+str+str;
        else {
            String front = str.substring(0,3);
        return front+front+front;}
    }

    public String backAround(String str) {
        return (str.charAt(str.length()-1) + str + str.charAt(str.length()-1));
    }

    public boolean or35(int n) {
        return (n%3 ==0 ||n%5 ==0) ;

    }

    public String front22(String str) {
        if (str.length() > 2) return (str.substring(0,2) + str +str.substring(0,2)) ;
        else return str +str +str;
    }

    public boolean startHi(String str) {
        return str.length() < 2 ? false : str.substring(0,2).equals("hi");
    }

    public boolean icyHot(int temp1, int temp2) {
        if ((temp1 < 0 && temp2 > 100) || (temp1 > 100 && temp2 < 0)) {
            return true;
        } else {
            return false;
        }
    }

    public boolean in1020(int a, int b) {
        return (a >= 10 && a <=20 || b >= 10 && b <=20);
    }

    public boolean loneTeen(int a, int b) {
        boolean aTeen =(a>=13 && a<=19);
        boolean bTeen = (b >=13 && b <=19);
        return ( aTeen && !bTeen) || (!aTeen && bTeen);
    }


    public String delDel(String str) {
        if ( str.length()>3){
            if (str.substring(1,4).equals("del")){
                return (str.charAt(0) + str.substring(4));
            }
            else return str;}
        else return str;
    }


    public boolean mixStart(String str) {
        if (str.length() >2){
            if ( (str.substring(1,3).equals("ix")))
            {
                return true;
            }
            else return false;
        }
        else return false;
    }


    public String startOz(String str) {
        String result = "";
        int a = 0x001;
        int b = 0x010;
        int c = a & b; // 0x011


        if (str.length()>=1 && str.charAt(0)=='o'){
            result +=str.charAt(0);
        }
        if (str.length()>=2 && str.charAt(1) =='z'){
            result+=str.charAt(1);
        }
        return result;
    }

    public int intMax(int a, int b, int c) {
        int max;
        if (a >b){
            max =a;
        }
        else max =b;
        if (c > max){
            max =c;
        }
        return max;
    }

    public int close10(int a, int b) {

        if (Math.abs(a -10) > Math.abs(b -10)){
            return b;
        }
        if  (Math.abs(a -10) == Math.abs(b -10))
            return 0;
        else return a;
    }

    public boolean in3050(int a, int b) {
        boolean n3040 =(a>=30 && a<=40 && b>=30 && b<=40 );
        boolean n3050 =(a>=40 && a<=50 && b>=40 && b<=50 );
        if (n3040 ||n3050) return true;
        else return false;

    }

    public int max1020(int a, int b) {
        boolean rangeA =(a >=10 && a<=20);
        boolean rangeB =(b>=10 && b <=20);

        if (rangeA && rangeB)
        {
            if (a >b) return a;
            else return b;
        }
        // if (rangeA || rangeB) return Math.max(a,b);
        if (rangeA && !rangeB) return a;
        else if (!rangeA && !rangeB) return 0;
        else return b;
    }


    public boolean stringE(String str) {
        int count =0;
        for (int i=0;i < str.length();i++)
        {
            if (str.charAt(i)=='e') count++;
        }
        return (count>=1&&count <=3);
    }


    public boolean lastDigit(int a, int b) {
        return (a%10 ==b%10);
    }


    public String endUp(String str) {
        if (str.length() >2)
        {
            return str.substring(0,str.length()-3)+str.substring(str.length()-3).toUpperCase();

        }
        else return str.toUpperCase();
    }

    public String everyNth(String str, int n) {
        String result ="";
        for (int i=0; i < str.length(); i=i + n){
            result +=str.charAt(i);
        }
        return result;
    }



}
